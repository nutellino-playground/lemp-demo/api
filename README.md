# Api Demo

## Installing composer packages

```bash
docker run --rm --interactive --tty --volume $PWD:/app composer install --ignore-platform-reqs
```


## Updating composer packages

```bash
docker run --rm --interactive --tty --volume $PWD:/app composer update --ignore-platform-reqs

```

