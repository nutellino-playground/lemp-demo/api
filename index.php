<?php
error_reporting(E_ALL);
require_once __DIR__ . '/vendor/autoload.php';
date_default_timezone_set('Europe/Rome');

use Nutellino\Common\Database as Database;
use Pecee\SimpleRouter\SimpleRouter as Router;
use Pecee\Http\Response;
use Pecee\Http\Request;


Router::get('/', function () {

    return "";
});

Router::get('/healthcheck', function () {
    return "WORKS";
});


Router::get('/getdata', function () {
    $db = new Database();
    $db = $db->getConnection();

    $example = new \Nutellino\Common\Example($db);

    Router::response()->json([
        'results' => $example->getAll(),
        'errors' => []
    ]);

});


Router::start();

